//Declare dependencies and model
const Member = require("../models/members");
const express = require("express");
const router = express.Router(); //to handle routing
const bcrypt = require('bcryptjs');
const auth = require("../middleware/auth");
const multer = require("multer");
const sharp = require("sharp");


//-------MEMBERS
// 1) CREATE MEMBER 
router.post("/", async (req, res) =>{
	const member = await new Member(req.body);
	// const memberRole = Member.find({role: 1});
	// // console.log(memberRole)
	// if(!memberRole){
	// // 	res.status(403).send(e)
	// 	console.log("test")
	// }
	// member.save().then(() => {res.send(member)})
	// .catch((e) => {res.status(400).send(e)})
	// try{
	
	// 	await member.save();
	// 	res.send(member); //or you can use res.status(201).send(member)
	// }catch(e){
	// 	res.status(400).send(e)
	// }

	try{
		console.log("create")
		if(!member) {
			return res.send("no member created")
		}
		await member.save()
		console.log(member.role)
		res.sendStatus(200)

	} catch(e) {
		res.status(500).send(e)
	}
	

})
//2 GET ALL MEMBER

router.get("/", auth, async (req, res)=>{
	// Member.find().then((members) => { return res.status(200).send(members)})
	// .catch((e) => { return res.status(500).send(e)})

	// console.log(req.member.role)
	const match = {};
	const sort = {}
	if(req.query.position){
		match.position = req.query.position}

	if(req.query.sortBy){
		const parts = req.query.sortBy.split(":");
		sort[parts[0]] = parts[1] === "desc" ? -1 : 1 //sort desc = -1 if not 1 for asc
	}

	try {
		await req.member.populate({
			path: "members",
			match,
			options: {
				limit: parseInt(req.query.limit),
				skip: parseInt(req.query.skip),
				sort //sort: sort
			}
		}).execPopulate();
		res.status(200).send(req.member)
	} catch(e){
		return res.status(404).send(e)
	}
}) 

//7) GET LOGIN USER'S PROFILE

router.get("/me", auth, async (req, res) => {
	console.log("test");
	// res.send(req.member)
	// res.send(req.token)
	res.send(req.member)
})


//3)GET ONE MEMBER
router.get("/:id", auth, async (req, res) =>{
	const _id = req.params.id;
	// Member.findById(_id).then((member) => {if(!member){
	// 	return res.status(404).send(e)
	// } return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const member = await Member.findById(_id);
		if(!member){
		return res.status(404).send("WALANG MAHANAP NA member")};
		res.send(member)
	}catch(e){
		res.status(500).send(e)
	}
})
//4)UPDATE ONE MEMBER (update own profile)
router.patch("/me", auth, async (req, res) =>{
	// const _id = req.params.id
	const updates = Object.keys(req.body);

	const allowedUpdates = ["firstName", "lastName", "position", "password", "teamId", "role"];

	const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

	if(!isValidUpdate){
		return res.status(400).send({error: "Invalid update"})
	}
	// Member.findByIdAndUpdate(_id, req.body, {new:true}).then((member) => {
	// 	if(!member){return res.status(404).send(e)}
	// 	return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		// const member = await Member.findByIdAndUpdate(_id, req.body, { new:true }); 

		// const member = await Member.findById(_id);
		updates.map(update => (req.member[update] = req.body[update]));

		// if(!member){
		// return res.status(404).send("WALANG MAHANAP")};

		await req.member.save();

		res.send(req.member);
	} catch(e){
		return res.status(500).send(e)
		}
})
//5)DELETE ONE MEMBER
router.delete("/:id", auth, async (req, res)=> {
	const _id = req.params.id;
	// Member.findByIdAndDelete(_id).then((member) => {if(!member){return res.status(404).send(e)}
	// 	return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const member = await Member.findByIdAndDelete(_id);
		if(!member){
			return res.status(404).send("Member doesn't exist or whatever")
		}	res.send(member)
	}catch(e){
		res.status(500).send(e.message)
	}
})

//6) LOGIN
router.post("/login", async(req, res) => {
	try{
		//submit email and password
		// const member = await Member.findByCredentials(req.body.email, req.body.password);
	const member = await Member.findOne({$or: [{email: req.body.email}, {username: req.body.email}]})



		if(!member){
			return res.send({"message":"Invalid login credentials!"})} 

		const isMatch = await bcrypt.compare(req.body.password, member.password);

		if(!isMatch){
			return res.send({"message":"Invalid login credentials!"}) //for now
		}
		//generate token

		const token = await member.generateAuthToken();

		res.send({member, token});

	} catch(e){
		res.status(500).send(e);
	}
})

//8) LOGOUT

//9) LOGOUT ALL

router.post('/logout', auth, async (req, res) => {
  try {
  	req.member.tokens.splice(0);
  	await req.member.save();
  	res.send("Log out ka na sa lahat!")

    /*
      CLUES:
      Study the result in your console. 
      Create a contant variable called remainingTokens. Its value should be a method that filters 
      a member's token so that it doesn't include the req's token.
      Assign the result of remainingTokens to the member's token.
      Await saving of changes made to member.
      Send back the member
    */
  } catch (e) {
    //Send back status 500 and the error
  res.status(500).send(e);
  }
});

//===================================

//UPLOAD FILE

const upload = multer({
	// dest: "images/teams",
	limits: {
	fileSize: 1000000
	},
	fileFilter(req,file, cb){
		if(!file.originalname.match(/\.(jpg|jpeg|png|PNG|JPEG)$/)){
			return cb(new Error("Please upload an image only."))
		}
		cb(undefined, true)
	}
});

//localhost:port/members/upload
router.post("/upload", auth, upload.single("label-upload"), async(req, res) =>{
		
		const buffer = await sharp(req.file.buffer).resize({
			width: 50,
			height: 50
		})
		.jpeg()
		.toBuffer();

		// req.member.profilePic = req.file.buffer;
		req.member.profilePic = buffer;
		await req.member.save();
		// res.send({message: "Successfully uploaded image!"});
		res.send(req.member);

}, (error, req, res, next) => {
	res.status(400).send({error: error.message})
});

//delete a photo

router.delete("/upload", auth, async (req, res) =>{

})

//DISPLAY AN IMAGE -> who can view profilePics? Anyone: none logged in and not owners
//localhost:port/members/:id/upload
router.get("/:id/upload", async(req, res) =>{
	try{
		const member = await Member.findById(req.params.id);

		if(!member || !member.profilePic){
			return res.status(404).send("Member or Profile Pic Doesn't exist")
		}

		//SEND BACK THE CORRECT DATA
		//TELL THE CLIENT WHAT TYPE OF DATA IT WILL RECEIVE
		res.set("Content-Type", "image/png")
		res.send(member.profilePic);
	}catch(e){
		res.status(500).send(e)
	}
})

module.exports = router;